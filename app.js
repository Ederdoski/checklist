var express = require('express');
var http = require('http');
var app = express();
var cors = require('cors');

//--- JSON
var bodyParser = require('body-parser')
app.use(bodyParser.json() );      
app.use(bodyParser.urlencoded({    
  extended: true
})); 

var router = express.Router();
var routerLogin = require('./routes/login');
var routerUsers = require('./routes/users');
var routerUpload = require('./routes/upload');


app.use(cors());
app.use('/checklist-services', router);
app.use('/login', routerLogin);
app.use('/users', routerUsers);
app.use('/upload', routerUpload);

router.use(function(req, res, next) {

  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  next();
});

router.get('/', function (req, res) {
 
  res.json("CheckList Running !!!");

});


router.post('/', function (req, res) {
 
  res.json({"response":"success","info":"asas"});

});

//--- Configurando Puerto
var port = process.env.PORT || 3000;

//--- Iniciando Servidor

http.createServer(app).listen(port, function(){
  console.log('Servido Corriendo en el puerto: %d', port);
});