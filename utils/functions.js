function checkUser(pool, data, callback){

        var fields = "id";
        var values = pool.escape(data.id_user);
        var sql = "SELECT " + fields + " FROM users WHERE id = " + values;
              pool.getConnection(function(err, connection) {

                pool.query(sql, function(error, row) 
                {
                    if(error)
                    {
                         console.log('Model Error: '+error);
                         callback(false);
                    }
                    else
                    {   
                        if(row[0] != null)
                        {
                            callback(true);
                        }
                        else
                        {
                            callback(false);
                        }
                    }
                });
            }); 
}

function checkPlaces(pool, data, callback){

        var fields = "id";
        var values = pool.escape(data.id_place);
        var sql = "SELECT " + fields + " FROM places WHERE id = " + values;
              pool.getConnection(function(err, connection) {

                pool.query(sql, function(error, row) 
                {
                    if(error)
                    {
                         console.log('Model Error: '+error);
                         callback(false);
                    }
                    else
                    {   
                        if(row[0] != null)
                        {
                            callback(true);
                        }
                        else
                        {
                            callback(false);
                        }
                    }
                });
            }); 
}

exports.checkUser = checkUser;
exports.checkPlaces = checkPlaces;
