var express = require('express');
var router = express.Router();
var app = express(); 

//--- Modelos

var userCtrl = require('../models/users');

//--- Acciones 

router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

router.get('/:id_place',function(req,res){

	if (req.params.id_place != null){

			data = {
				  		id_place : req.params.id_place
					}

		    userCtrl.getPoll(data,function(error, data)
			{
				if(data)
				{
					res.status(200)
					   .json(data);
				}

				if(error){
					res.status(200)
					   .json({"response":"fail","msg":"Error ciudad invalida"});
				
				}
				
				if (!error && !data){
					res.status(500)
					   .json({"response":"success","msg":"Error Interno"});
				}
			});
		}else{
			res.status(500)
				   .json({"response":"success","msg":"Parametro Principal Invalido"});
		}
});

router.post('/',function(req,res){

		var data;

			data = {
				id_user : req.body.id,
				name : req.body.name,
				user : req.body.user,
				pass : req.body.pass,
				id_place : req.body.id_place,
				lastname_p : req.body.lastname_p,
				lastname_m : req.body.lastname_m
			}

		userCtrl.proccessRequest(data,function(error, data)
		{
			if(data)
			{
				res.status(200)
				   .json({"response":"success","info":data});
			}

			if(error){
				res.status(200)
				   .json({"response":"fail","msg":"Error Usuario Existente"});
			
			}
			
			if (!error && !data){
				res.status(500)
				   .json({"response":"success","msg":"Error Interno"});
			}
		});
	});

module.exports = router;

