var express = require('express');
var router = express.Router();
var app = express(); 
var http = require('http');
var formidable = require('formidable');

//--- Modelos


//--- Acciones 

router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


router.get('/',function(req,res){

   res.writeHead(200, {'Content-Type': 'text/html'});
   res.end(dibujarFormulario());

});

router.post('/',function(req,res){

      var incoming = new formidable.IncomingForm();
      var error = false;
      incoming.uploadDir = 'upload/incident';


      incoming.onPart = function (part) {
          if(!part.filename || part.filename.match(/\.(jpg|jpeg|png)$/i)) {
              this.handlePart(part);
          }
          else {
               error = true;
          }
      }

      //---Response Parser
      incoming.parse(req, function(err, fields, files) {
         if(error == false){
            if(files.file.size == 0 || fields.incident == ""){
               error = true;
            }
         }
      });

      //---OnError
      incoming.on('error', function(err) {
            error = true;
      });

      //--OnUpload
      incoming.on('file', function(field, file){
          console.log('Archivo recibido');
      });

      //--AfterSave
      incoming.on('fileBegin', function(field, file){
         if(file.name){
            file.path += '_' + file.name;
         }
      });

      //---OnSave
      incoming.on('end', function(){
         if(error){
            res.status(500)
               .json({"response":"fail","msg":"Parametros invalidos o archivo no soportado"});
         }else{
            res.status(200)
               .json({"response":"success","msg":"Archivo cargado exitosamente"});
         }
      });

});

function dibujarFormulario(){
   var html = '<form method="post" enctype="multipart/form-data">';
   html += '<label> Archivo: </label>';
   html += '<input type="file" name="file" />';
   html += '<input type="submit" value="Subir" />';
   html += 'Nombre: <INPUT type="text" name="nombre"><BR>'
   html += 'Apellido: <INPUT type="text" name="apellido"><BR>'
   html += '</form>';
   return html;
}

module.exports = router;
