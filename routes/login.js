var express = require('express');
var router = express.Router();
var app = express(); 
	
//--- Modelos

var loginModel = require('../models/login');

//--- Acciones 

router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  next();
});

router.post("/", function(req,res){

		var data = {
			user : req.body.user,
			pass : req.body.pass
		};
		
		loginModel.attempLogin(data,function(error, data)
		{
			if(data)
			{
				res.status(200)
				   .json({"response":"success","info":data});
			}

			if(!data){
				res.status(200)
					   .json({"response":"fail","msg":"Usuario o Contraseña invalido","info:":data});
			}

			if (error){
				res.status(500)
				   .json({"response":"success","msg":"Error Interno"});
			}
		});
	});

module.exports = router;