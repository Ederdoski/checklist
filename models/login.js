
var pool  = require('./connection');

//--- Funciones 

var functions = require('../utils/functions');

var loginModel = {};

loginModel.attempLogin = function(data,callback)
{
	if (pool) 
	{

		var fields = "id_place";

		var values = [pool.escape(data.user), 
					  pool.escape(data.pass)];

		var sql = 'SELECT ' + fields + ' FROM users WHERE user = ' + values[0] +
				  ' AND pass = ' + values[1];
		
		pool.getConnection(function(err, connection) {
			
			connection.query(sql, function(error, row) 
			{
				connection.release();

				if(error)
				{
					console.log('Error : '+error);
				}
				else
				{	
					if(row.length == 0){

						callback(null, false);

					}else{
                        
						callback(null, row);
					}
				}
			});
		});
	}
}

module.exports = loginModel;
