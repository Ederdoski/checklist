
/*var mysql = require('mysql');
var pool  = mysql.createPool({
  connectionLimit : 100,
  host            : '23.229.191.133',
  user            : 'eder1234',
  password        : 'eder1234',
  database        : 'node12345'
});
*/

'use strict';

// [START setup]
const express = require('express');
const mysql = require('mysql');

const app = express();
app.enable('trust proxy');
// [END setup]

// [START connect]
var config = {
  connectionLimit : 100,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE
};

if (process.env.INSTANCE_CONNECTION_NAME) {
  config.socketPath = `/cloudsql/${process.env.INSTANCE_CONNECTION_NAME}`;
}

// Connect to the database
const pool = mysql.createPool(config);
// [END connect]

module.exports = pool;