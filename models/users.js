var pool  = require('./connection');

//--- Funciones 

var functions = require('../utils/functions');

var usersModel = {};

usersModel.getPoll = function(data, callback)
{
    if (pool) 
    {
        functions.checkPlaces(pool, data, function(places){
            
            if(places){

                pool.getConnection(function(err, connection) {

                var fields = "id";
                var values = pool.escape(data.id_place);

                var sql = 'SELECT ' +fields+ ' FROM users WHERE id_place = '+ values +' AND type = "poll"';

                    connection.query(sql, function(error, row) 
                    {
                        if(error)
                        {
                             console.log('Model Error: '+error);
                             callback(true,null);
                        }
                        else
                        {
                            callback(false,row);
                        }
                    });
                });  
            }else{
                callback(true,null);
            }
        });
    }
}

usersModel.proccessRequest = function(data,callback){

    if (pool) 
    {

        functions.checkUser(pool, data, function(user){

            if(!user)
            {  
                var fields;
                var values;          

                fields = "id, name, user, pass, id_place, lastname_p, lastname_m";

                values = pool.escape(data.id_user) +","+ 
                         pool.escape(data.name) +","+
                         pool.escape(data.user) +","+
                         pool.escape(data.pass) +","+
                         pool.escape(data.id_place) +","+
                         pool.escape(data.lastname_p) +","+
                         pool.escape(data.lastname_m);


                var sql = "INSERT INTO users ("+fields+") VALUES ("+values+")";

                  pool.getConnection(function(err, connection) {

                    connection.query(sql, function(error, row) 
                    {
                        if(error)
                        {
                             console.log('Model Error: '+error);
                             callback(true,null);
                        }
                        else
                        {
                            callback(false,"Usuario insertado exitosamente");
                        }
                    });
                });   
            }else{
                callback(true,null);
            }
        });
    }
}

module.exports = usersModel;